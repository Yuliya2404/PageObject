from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from page_object.main_page import MainPage


class ConfirmationPage(MainPage):
    @property
    def confirmation_page(self):
        return self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "integration_game_topBar_nav")))