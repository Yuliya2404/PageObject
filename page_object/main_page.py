
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class MainPage:
    def __init__(self, web_driver):
        self.web_driver = web_driver
        self.wait = WebDriverWait(self.web_driver, 20)
        self.actions = ActionChains(self.web_driver)

    @property
    def logo(self):
        return self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "topBar_logo")))

    @property
    def find_facebook_element(self):
        return self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "a[title=facebook]")))

    @property
    def find_email_element(self):
        return self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#integration_game_login_email")))

    def drag_and_drop(self):
        return self.actions.click_and_hold(self.find_facebook_element).pause(2).move_to_element(self.find_email_element).release(self.find_email_element).click(self.find_email_element).perform()

    @property
    def find_signup_button(self):
        return self.wait.until(EC.element_to_be_clickable((By.ID, "integration_game_signUp")))

    def go_to_signup_window(self):
        self.find_signup_button.click()

    @property
    def signup_window(self):
        return self.wait.until(EC.visibility_of_element_located((By.ID, "integration_loginG")))

    @property
    def email_field(self):
        return self.wait.until(EC.element_to_be_clickable((By.ID, "integration_reg_email")))

    @property
    def password_field(self):
        return self.wait.until(EC.element_to_be_clickable((By.ID, "integration_reg_password")))

    @property
    def password_confirmation_field(self):
        return self.wait.until(EC.element_to_be_clickable((By.ID, "integration_reg_password_re")))

    @property
    def signup_button(self):
        return self.wait.until(EC.element_to_be_clickable((By.ID, "integration_signBtn")))

    @property
    def find_checkbox(self):
        return self.wait.until(EC.element_to_be_clickable((By.NAME, "policy0")))

    def enter_email(self, email):
        self.email_field.send_keys(email)
        return self

    def enter_password(self, password):
        self.password_field.send_keys(password)
        return self

    def confirm_password(self, password):
        self.password_confirmation_field.send_keys(password)
        return self

    def select_the_checkbox(self):
        if not self.find_checkbox.is_selected():
            self.find_checkbox.click()
            return self

    def click_signup_button(self):
        from page_object.confirmation_page import ConfirmationPage
        self.signup_button.click()

        return ConfirmationPage(self.web_driver)
