

from page_object.main_page import MainPage


class TestPageObject:

    def test_logo(self, main_page: MainPage):
        assert main_page.logo.is_displayed()

    def test_drag_and_drop(self, main_page: MainPage):
        assert main_page.find_facebook_element.is_enabled
        assert main_page.find_email_element.is_displayed
        main_page.drag_and_drop()

    def test_signup(self, main_page: MainPage, email, password):
        main_page.go_to_signup_window()
        assert main_page.signup_window.is_displayed, f"No signup window"
        assert main_page.signup_window.find_element_by_id("integration_loginCloseBtn").is_displayed, f"The X button is not displayed"
        assert main_page.find_checkbox.is_displayed, f"The checkbox is not displayed"

        registration = main_page.enter_email(email).enter_password(password).confirm_password(password).select_the_checkbox().click_signup_button()

        assert registration.confirmation_page.is_displayed, f"Registration failed"



