import random
import string

import pytest


def get_random_string(size = 10):
    return ''.join(random.choice(string.ascii_letters + string.digits) for x in range(size))

@pytest.fixture
def email():
    return f"{get_random_string()}@gmail.com"

@pytest.fixture
def password():
    return get_random_string()
