import pytest
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver import Chrome

from page_object.main_page import MainPage


@pytest.fixture
def main_page():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()
    chrome_browser.get("https://loa3.gtarcade.com/")
    yield MainPage(chrome_browser)
    chrome_browser.quit()

